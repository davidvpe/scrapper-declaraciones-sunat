# Scrapper Declaraciones Sunat

Web scrapping de declaraciones y pagos de la Sunat

## Configuracion

```bash
# Environment variables
DYNAMODB_TABLE=TABLE
S3_BUCKET=BUCKET
SUNAT_RUC=10000000000
SUNAT_USER=user
SUNAT_PASS=password
DATE_FROM=2019-01-01
DATE_TO=2019-12-31
```