declare module 'puppeteer-extra-plugin-user-preferences' {
  export = defaultExport
  declare function defaultExport(opts?: any): UserPreferencesPlugin
  declare const UserPreferencesPlugin_base: typeof import('puppeteer-extra-plugin').PuppeteerExtraPlugin

  declare class UserPreferencesPlugin extends UserPreferencesPlugin_base {
    constructor(opts?: any)
    onPageCreated(page: any): Promise<void>
  }
}
//Create declare for process.env variables

declare namespace NodeJS {
  export interface ProcessEnv {
    DYNAMODB_TABLE: string
    S3_BUCKET: string
    SUNAT_RUC: string
    SUNAT_USER: string
    SUNAT_PASS: string
    DATE_FROM: string
    DATE_TO?: string
  }
}
