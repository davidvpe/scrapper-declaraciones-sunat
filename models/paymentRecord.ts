export type PaymentRecord = {
  id: string
  fechaPresentacion: string
  periodo: string
  codigoFormulario: string
  descripcionFormulario: string
  numOrder: string
  detalle_func: string
  ruc: string
}
