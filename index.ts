import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import UserPreferencesPlugin from 'puppeteer-extra-plugin-user-preferences'

import dotenv from 'dotenv'

dotenv.config()

import { clickOnBuscar, Credentials, fillTextBoxs as fillCredentials, getRecordFromDate } from './util/parser'

const creds: Credentials = {
  ruc: process.env.SUNAT_RUC,
  user: process.env.SUNAT_USER,
  pass: process.env.SUNAT_PASS
}

const main = async () => {
  console.time('tiempo de ejecución')
  puppeteer.use(StealthPlugin()).use(
    UserPreferencesPlugin({
      userPrefs: {
        download: {
          prompt_for_download: false,
          open_pdf_in_system_reader: true,
          default_directory: '/tmp'
        },
        plugins: {
          always_open_pdf_externally: false
        },
        devtools: {
          preferences: {
            'console_log.preserve-log': '"true"'
          }
        }
      }
    })
  )
  console.log('Environment variables', process.env)

  const browser = await puppeteer.launch({ args: ['--no-sandbox'], headless: false, timeout: 0, devtools: true })

  const page = await browser.newPage()

  await page.goto('https://e-menu.sunat.gob.pe/cl-ti-itmenu/MenuInternet.htm')
  await page.waitForNavigation({ waitUntil: 'networkidle0' })
  await fillCredentials(page, creds)
  await clickOnBuscar(page)
  await page.waitForNavigation({ waitUntil: 'networkidle0' })

  await page.evaluate(() => {
    window.onbeforeunload = null
  })
  await page.goto('https://e-menu.sunat.gob.pe/cl-ti-itmenu/MenuInternet.htm?action=iconExecute&code=12.1.1.1.4&s=ww1')

  const dateTo = process.env.DATE_TO
  if (!dateTo) await getRecordFromDate(page, new Date(process.env.DATE_FROM))
  else await getRecordFromDate(page, new Date(process.env.DATE_FROM), new Date(dateTo))

  browser.close()
  console.timeEnd('tiempo de ejecución')
}

main()
