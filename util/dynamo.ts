import { DynamoDBClient, PutItemCommand, AttributeValue } from '@aws-sdk/client-dynamodb'
import { PaymentRecord } from '../models/paymentRecord'

const client = new DynamoDBClient({})

const tableName = process.env.DYNAMODB_TABLE

export const insertRecord = (record: PaymentRecord) => {
  const item: Record<keyof Omit<PaymentRecord, 'id' | 'detalle_func'>, AttributeValue> = {
    numOrder: { S: record.numOrder },
    codigoFormulario: { S: record.codigoFormulario },
    descripcionFormulario: { S: record.descripcionFormulario },
    fechaPresentacion: { S: record.fechaPresentacion },
    periodo: { S: record.periodo },
    ruc: { S: process.env.SUNAT_RUC }
  }

  return client.send(
    new PutItemCommand({
      TableName: tableName,
      Item: item
    })
  )
}
