import { Page } from 'puppeteer'
import { PaymentRecord } from '../models/paymentRecord'
import { formatDate } from './formatter'
import { checkIfFileExistsInBucket, uploadFileAtPath } from './s3'
import fs from 'fs/promises'
import { insertRecord } from './dynamo'
export type Credentials = {
  ruc: string
  user: string
  pass: string
}

export const getRecordFromDate = async (page: Page, from: Date, to: Date = new Date()) => {
  //if from and to are more than 5 months apart, then call this function for each 5 months

  const maxLength = 1000 * 60 * 60 * 24 * 30 * 5

  let actualFrom = from
  let actualTo = to.getTime() - from.getTime() > maxLength ? new Date(from.getTime() + maxLength) : to

  do {
    const fechaDesde = formatDate(actualFrom)
    const fechaHasta = formatDate(actualTo)

    await fillDateBoxes(page, fechaDesde, fechaHasta)

    await clickOnBuscarDeudas(page)
    try {
      await page.waitForNetworkIdle()
    } catch (e) {
      console.log('Error en espera de red, continuando')
    }

    // await interceptOpenURL(page)
    await downloadFiles(page)

    if (actualFrom.getTime() === actualTo.getTime()) break
    actualFrom = actualTo
    actualTo = actualTo.getTime() + maxLength > to.getTime() ? to : new Date(actualTo.getTime() + maxLength)
  } while (actualTo.getTime() <= to.getTime())
}

export const clickOnBuscar = async (page: Page) => {
  return page.evaluate(() => {
    const button = document.querySelector('#btnAceptar') as HTMLButtonElement
    if (button) {
      button.click()
    }
  })
}

export const fillTextBoxs = async (page: Page, creds: Credentials) => {
  return page.evaluate((creds) => {
    const values = Object.values(creds)
    const fields = ['txtRuc', 'txtUsuario', 'txtContrasena']

    console.log(values)
    fields.forEach((id, index) => {
      const input = document.querySelector(`#${id}`) as HTMLInputElement
      console.log('input', input)
      if (input) {
        input.value = values[index]
      }
    })
  }, creds)
}

export const downloadFiles = async (page: Page) => {
  let files: PaymentRecord[] = []
  let pageFiles: PaymentRecord[] = []
  let count = 0
  do {
    try {
      await page.evaluate(`paginacion(10,${count})`)

      await new Promise((resolve) => setTimeout(resolve, 1000))

      pageFiles = await page.evaluate(() => {
        console.log('Evaluating table')
        const headers = document.querySelectorAll('th.beta') as NodeListOf<HTMLTableCellElement>
        console.log('headers', headers)
        if (headers.length < 3) return []

        const rows = Array.from(document.querySelectorAll('tr')).filter(
          (r) => r.querySelectorAll('td').length === headers.length
        )

        console.log('rows', rows)

        return rows.map((row) => {
          const columns = row.querySelectorAll('td')
          let values: Record<string, string> = {}
          Array.from(headers).forEach((header, index, arr) => {
            const value = columns[index].innerText
            if (arr.length - 1 > index) {
              switch (header.innerText) {
                case '':
                  values.id = value
                  break
                case 'Fec. Pres.':
                  values.fechaPresentacion = value
                  break
                case 'Periodo':
                  values.periodo = value
                  break
                case 'Cod.Form.':
                  values.codigoFormulario = value
                  break
                case 'Desc.Form.':
                  values.descripcionFormulario = value
                  break
                case 'N° Orden':
                  values.numOrder = value
                  break
              }
            } else {
              values.detalle_func = columns[index].querySelector('a')?.href.split('javascript:')[1] ?? ''
            }
          })
          return values as PaymentRecord
        })
      })
      count++
      files.push(...pageFiles)
    } catch (e) {
      console.log('Error cargando js', e)
      await downloadFiles(page)
      files = []
      break
    }
  } while (pageFiles.length > 0)

  let index = 0
  for (let file of files) {
    index++
    console.log('Processing file', index + 1, 'of', files.length)
    const downloaded = await checkIfFileExistsInBucket(file)

    if (!downloaded) {
      console.log('⬇️...', file.numOrder)
      const pdfName = await downloadPdf(page, file)

      if (pdfName) {
        const pdfLocation = `/tmp/${pdfName}`
        await uploadFileAtPath(pdfLocation, file)
        await fs.unlink(pdfLocation)

        await insertRecord(file)
      }
    }
    console.log('✅', file.numOrder)
    continue
  }
}

export const fillDateBoxes = (page: Page, fechaDesde: string, fechaHasta: string) => {
  console.log('Empezando busqueda de fechas', fechaDesde, fechaHasta)

  return page.evaluate(
    (fechaDesde, fechaHasta) => {
      const values = [fechaDesde, fechaHasta]
      const fields = ['fechaDesde', 'fechaHasta']

      console.log(values)
      fields.forEach((id, index) => {
        const input = document.querySelector(`input[name='${id}']`) as HTMLInputElement
        console.log('input', input)
        if (input) {
          input.value = values[index]
        }
      })
    },
    fechaDesde,
    fechaHasta
  )
}

export const clickOnBuscarDeudas = async (page: Page, attempt = 1) => {
  if (attempt < 11)
    try {
      await page.evaluate('consulta(document.formFechas);')
    } catch (e) {
      console.log('Ocurrio un error, Intento: ', attempt)
      await new Promise((resolve) => setTimeout(resolve, 1000))
      await clickOnBuscarDeudas(page, attempt + 1)
    }
}

export const downloadPdf = async (page: Page, file: PaymentRecord) => {
  const js = file.detalle_func
  let name: string | undefined
  if (js.startsWith('comprobanteNSIR') || js.startsWith('constanciaNP')) {
    name = await downloadPdfFileFromJS(page, file)
  } else if (js.startsWith('comprobante')) {
    name = await downloadFromComprobante(page, file)
  } else {
    console.log('unsupported', js)
    return undefined
  }

  await new Promise((resolve) => setTimeout(resolve, 3000)) //Esperando tiempo prudente para que se descargue o genere el archivo
  return name
}

export const downloadFromComprobante = async (page: Page, file: PaymentRecord) => {
  const js = file.detalle_func
  await page.evaluate(js)
  await page
    .waitForNetworkIdle({ timeout: 10000 })
    .catch((e) => console.log('Error esperando a que se cargue la pagina', e))
  const filePath = file.numOrder + '.pdf'
  await page.pdf({ path: '/tmp/' + filePath })
  await page.goBack()
  return filePath
}

export const downloadPdfFileFromJS = async (page: Page, file: PaymentRecord) => {
  const js = file.detalle_func

  const files = await fs.readdir('/tmp')
  const pdfs = files.filter((f) => f.endsWith('.pdf'))
  for (let pdf of pdfs) {
    await fs.unlink(`/tmp/${pdf}`)
  }

  await page.evaluate(js)

  const foundPdf = await new Promise<string>(async (resolve) => {
    const interval = setInterval(async () => {
      const files = await fs.readdir('/tmp')
      const foundFile = files.find((f) => f.endsWith('.pdf'))
      if (foundFile) {
        clearInterval(interval)
        resolve(foundFile)
      }
    }, 500)
  })

  return foundPdf
}
