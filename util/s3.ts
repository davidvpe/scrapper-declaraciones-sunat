import { S3Client, PutObjectCommand, HeadObjectCommand } from '@aws-sdk/client-s3'
import fs from 'fs/promises'
import path from 'path'
import { PaymentRecord } from '../models/paymentRecord'
const client = new S3Client({})

export const uploadFileAtPath = async (filePath: string, file: PaymentRecord) => {
  const bucket = process.env.S3_BUCKET
  const key = getS3Key(file)
  const blob = await fs.readFile(filePath)
  await client.send(new PutObjectCommand({ Bucket: bucket, Key: key, Body: blob }))
}

export const checkIfFileExistsInBucket = async (file: PaymentRecord) => {
  const bucket = process.env.S3_BUCKET

  const key = getS3Key(file)
  try {
    await client.send(new HeadObjectCommand({ Bucket: bucket, Key: key }))
    return true
  } catch (e) {
    return false
  }
}

const getS3Key = (file: PaymentRecord) => {
  const user = process.env.SUNAT_RUC
  return user + '/' + file.periodo + '-' + file.numOrder + '.pdf'
}
